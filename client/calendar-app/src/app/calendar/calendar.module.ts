import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarComponent } from './calendar.component';
import { SharedModule } from '../shared/shared.module';

import {MatTableModule} from '@angular/material/table';

import { CalendarViewComponent } from '../calendar-view/calendar-view.component';
import { DayViewComponent } from '../day-view/day-view.component'

@NgModule({
  declarations: [CalendarComponent, CalendarViewComponent, DayViewComponent],
  imports: [
    CommonModule,
    CalendarRoutingModule,
    SharedModule,

    MatTableModule
  ]
})
export class CalendarModule { }
