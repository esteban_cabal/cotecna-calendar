﻿using System;

namespace Cotecna.Calendar.Application
{
    public class Forecast
    {
        public DateTime Date { get; set; }
        public double Temperature { get; set; }
        public String WeatherCondition  { get; set; }
    }
}
