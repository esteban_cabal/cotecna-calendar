﻿namespace Cotecna.Calendar.Infraestructure.OpenWeatherMap.Model
{
    public class Wind
    {
        public double speed { get; set; }
        public int deg { get; set; }
    }
}
