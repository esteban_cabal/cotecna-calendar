import { Component, Input } from '@angular/core';
import { IDay } from '../calendar/day';

@Component({
  selector: 'cc-day-view',
  templateUrl: './day-view.component.html',
  styleUrls: ['./day-view.component.scss']
})
export class DayViewComponent {
  @Input() day: IDay;
  @Input() highlight: boolean;
  @Input() disabled: boolean;
}
