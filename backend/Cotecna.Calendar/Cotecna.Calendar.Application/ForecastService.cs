﻿using Cotecna.Calendar.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cotecna.Calendar.Application
{
    public class ForecastService : IForecastService
    {
        private readonly IWeatherService _weatherService;

        public ForecastService(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        public async Task<IEnumerable<Forecast>> GetForecast()
        {
            var result = await _weatherService.GetForecast();

            return result.GroupBy(f => f.Date.Date).Select(g => new Forecast
            {
                Date = g.Key,
                Temperature = g.FirstOrDefault().Temperature,
                WeatherCondition = g.FirstOrDefault().WeatherCondition
            });

        }
    }
}
