import { Component, OnInit } from '@angular/core';
import { IWeek } from './week';
import { CalendarService } from './calendar.service';
import { IOption } from '../shared/select/option';
import { WeatherForecastService } from './weather-forecast.service';
import { IForecast } from './forecast';

@Component({
  selector: 'cc-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  weekdays: string[];
  months: IOption[];
  years: IOption[]
  weeks: IWeek[];

  selectedYear: number;
  selectedMonth: number;

  erroMessage: string;

  constructor(
    private _calendarService: CalendarService,
    private _weatherService: WeatherForecastService) { }

  ngOnInit() {
    this.months = this._calendarService.getMonths().map((month, index) => ({ value: index, description: month }));
    this.years = this._calendarService.getYears().map(year => ({ value: year, description: String(year) }));

    this.weekdays = this._calendarService.getWeekdays();

    this.updateWeeks();
  }

  onSelectedYear(value: number) {
    this.selectedYear = value;
    this.updateWeeks();
  }

  onSelectedMonth(value: number) {
    this.selectedMonth = value;
    this.updateWeeks();
  }

  updateWeeks() {
    this.erroMessage = "";

    let month = this.selectedMonth !== undefined ? this.selectedMonth : this._calendarService.currentMonth;
    let year = this.selectedYear || this._calendarService.currentYear;

    this.weeks = this._calendarService.getWeeksFor(month, year);

    if (this._calendarService.isCurrentMonth(month, year)) {

      this._weatherService.getWeatherForecast().subscribe({
        next: forecasts => this.addWeatherForecast(forecasts),
        error: error => this.erroMessage = error
      });
    }
  }

  addWeatherForecast(forecasts: IForecast[]) {
    let weekIndex = this._calendarService.getTodayWeekIndex(this.weeks);
    let index = 0;

    for (let i = weekIndex; i < this.weeks.length || index < forecasts.length; i++) {
      let week = this.weeks[i];
      for (let j = 0; j < week.days.length; j++) {
        if (this._calendarService.isSameDay(forecasts[index].date, week.days[j].value)) {
          week.days[j].description = `${forecasts[index].weatherCondition} (${forecasts[index].temperature}º)`;
          index++;
        }
      }
    }
  }

}
