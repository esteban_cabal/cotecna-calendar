﻿namespace Cotecna.Calendar.Infraestructure.OpenWeatherMap.Model
{
    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }
}
