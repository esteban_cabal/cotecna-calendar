﻿using Cotecna.Calendar.Application;
using Cotecna.Calendar.Application.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Cotecna.Calendar.Infraestructure.OpenWeatherMap.Model;
using System.Linq;
using AutoMapper;

namespace Cotecna.Calendar.Infraestructure.OpenWeatherMap
{
    public class OpenWeatherMapService : IWeatherService
    {
        private readonly HttpClient _client;
        private readonly IMapper _mapper;

        private readonly string _apiKey;

        public OpenWeatherMapService(
            HttpClient client,
            IOptionsMonitor<OpenWeatherMapOptions> options,
            IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

            _apiKey = options.CurrentValue.Apikey;

            _client.BaseAddress = new Uri(options.CurrentValue.BaseAddress);
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        public async Task<IEnumerable<Forecast>> GetForecast()
        {
            // TODO: Accept cityId as parameter
            string barcelonaCityId = "3128760";

            var response = await _client.GetAsync($"?id={barcelonaCityId}&appid={_apiKey}&units=metric");
            
            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsAsync<RootObject>();

            return result.list.Select(x => _mapper.Map<Forecast>(x)).ToList();
        }
    }
}
