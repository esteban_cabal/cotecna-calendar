﻿namespace Cotecna.Calendar.Infraestructure.OpenWeatherMap
{
    public class OpenWeatherMapOptions
    {
        public string BaseAddress { get; set; }
        public string Apikey { get; set; }

        public OpenWeatherMapOptions() { }
    }
}
