import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectComponent } from './select/select.component';

import {MatSelectModule} from '@angular/material/select';
import { AlertComponent } from './alert/alert.component';


@NgModule({
  declarations: [ AlertComponent, SelectComponent],
  imports: [
    CommonModule,
    
     // Angular Material
    MatSelectModule
  ],
  exports: [
    AlertComponent,
    SelectComponent
  ]
})
export class SharedModule { }
