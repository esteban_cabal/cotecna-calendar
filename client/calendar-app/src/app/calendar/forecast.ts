export interface IForecast {
  date: string;
  temperature: number;
  weatherCondition: string;
}