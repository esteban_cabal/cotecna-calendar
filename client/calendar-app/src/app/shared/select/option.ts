export interface IOption {
  value: number;
  description: string;
}