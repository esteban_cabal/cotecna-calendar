﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cotecna.Calendar.Application
{
    public interface IForecastService
    {
        Task<IEnumerable<Forecast>> GetForecast();
    }
}
