﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cotecna.Calendar.Application.Interfaces
{
    public interface IWeatherService
    {
        Task<IEnumerable<Forecast>> GetForecast();
    }
}
