import { Component, Input, Output, EventEmitter, ɵConsole } from '@angular/core';
import { IOption } from './option';

@Component({
  selector: 'cc-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent {

  @Input() label: string;
  @Input() options: IOption[];

  @Output() selected = new EventEmitter<number>();

  onSelectionChange(value: number) {
    this.selected.emit(value);
  }
}
