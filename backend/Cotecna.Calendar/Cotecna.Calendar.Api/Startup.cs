﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cotecna.Calendar.Application;
using Cotecna.Calendar.Application.Interfaces;
using Cotecna.Calendar.Infraestructure.OpenWeatherMap;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cotecna.Calendar.Api
{
    public class Startup
    {
        readonly string LocalHostOrigins = "_localHostOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                  .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
            services.AddCors(options =>
            {
                options.AddPolicy(LocalHostOrigins,
                builder =>
                {
                    builder.WithOrigins("http://localhost:4200");
                });
            });

            services.AddAutoMapper();

            services.Configure<OpenWeatherMapOptions>(Configuration.GetSection("OpenWeatherMap"));

            services.AddHttpClient<IWeatherService, OpenWeatherMapService>();
            services.AddScoped<IForecastService, ForecastService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(LocalHostOrigins);
            }

            app.UseMvc();
        }
    }
}
