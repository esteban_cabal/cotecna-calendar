import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IForecast } from './forecast';

@Injectable({
  providedIn: 'root'
})
export class WeatherForecastService {

  forecastUrl = "http://localhost:5000/api/weather";

  constructor(private _http: HttpClient) { }

  getWeatherForecast(): Observable<IForecast[]> {  
    return this._http.get<IForecast[]>(this.forecastUrl).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'There was an error retrieving the weather information for the next 5 days; please try again later.');
  };

}
