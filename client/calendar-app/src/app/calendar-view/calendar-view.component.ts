import { Component, OnInit, Input } from '@angular/core';
import { IWeek } from '../calendar/week';

@Component({
  selector: 'cc-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss']
})
export class CalendarViewComponent implements OnInit {

  @Input() weekdays: string[];
  @Input() weeks: IWeek[];
  
  constructor() { }

  ngOnInit() {
  }

}
