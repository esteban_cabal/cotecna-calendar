import { Injectable } from '@angular/core';
import { IWeek } from './week';

import * as moment from 'moment';
import { Day } from './day';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  private _now: moment.Moment = moment().startOf('day');

  get currentDay(): number {
    return this._now.date();
  }

  get currentMonth(): number {
    return this._now.month();
  }

  get currentYear(): number {
    return this._now.year();
  }

  getWeekdays() {
    return moment.weekdaysShort();
  }

  getMonths() {
    return moment.months();
  }

  getYears() {
    return [2017, 2018, 2019, 2020];
  }

  getWeeksFor(month: number, year: number): IWeek[] {

    let firstDayOfMonth = moment({ month: month, year: year });
    let lastDayofMonth = firstDayOfMonth.clone().endOf('month').add(1, "week");

    let weeks = [];

    let firstDayOfWeek = firstDayOfMonth.clone().startOf('week');

    while (firstDayOfWeek.isBefore(lastDayofMonth)) {
      let week = this.createWeekFor(firstDayOfWeek.clone(), month, year);

      weeks.push(week);
      firstDayOfWeek.add(1, "week");
    }

    return weeks;
  }

  isCurrentMonth(month: number, year: number) {
    return month === this.currentMonth && year === this.currentYear;
  }

  getTodayWeekIndex(weeks: IWeek[]) {
    for (let i = 0; i < weeks.length; i++) {
      let week = weeks[i];
      for (let j = 0; j < week.days.length; j++) {
        if (week.days[j].value === this.currentDay) {
          return i;
        }
      }
    }
  }

  isSameDay(date: string, day: number) {
    return moment(date).date() === day;
  }

  private createWeekFor(firstDay: moment.Moment, month: number, year: number) {
    let week: IWeek = { days: [] };

    for (let i = 0; i < 7; i++) {
      let day = new Day(firstDay.date(),
        firstDay.isSame(this._now),
        firstDay.month() === month && firstDay.year() === year);

      week.days.push(day);
      firstDay.add(1, "d");
    }
    return week;
  }
}