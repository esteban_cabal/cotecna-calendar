﻿using Cotecna.Calendar.Application;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Cotecna.Calendar.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly IForecastService _forecastService;
        private readonly ILogger<WeatherController> _logger;

        public WeatherController(
            IForecastService forecastService,
            ILogger<WeatherController> logger)
        {
            _forecastService = forecastService ?? throw new ArgumentNullException(nameof(forecastService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public async Task<IActionResult> GetForecast()
        {
            try
            {
                _logger.LogInformation($"Getting weather forecast ");

                var result = await _forecastService.GetForecast();
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Exception trying to get weather forecast");
                return StatusCode(500);
            }
        }
    }
}
