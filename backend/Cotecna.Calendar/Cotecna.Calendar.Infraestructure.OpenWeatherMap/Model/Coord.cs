﻿namespace Cotecna.Calendar.Infraestructure.OpenWeatherMap.Model
{
    public class Coord
    {
        public double lat { get; set; }
        public double lon { get; set; }
    }
}
