﻿using AutoMapper;
using Cotecna.Calendar.Application;
using Cotecna.Calendar.Infraestructure.OpenWeatherMap.Model;
using System;
using System.Linq;

namespace Cotecna.Calendar.Infraestructure.OpenWeatherMap.Mapping
{
    public class ForecastProfile : Profile
    {
        public ForecastProfile()
        {
            CreateMap<List, Forecast>()                
                 .ForMember(dest => dest.Date,
                    opts => opts.MapFrom(s => DateTimeOffset.FromUnixTimeSeconds(s.dt).DateTime))
                 .ForMember(dest => dest.Temperature,
                    opts => opts.MapFrom(s => s.main.temp))
                .ForMember(dest => dest.WeatherCondition,
                    opts => opts.MapFrom(s => s.weather.FirstOrDefault().description));
        }
    }
}
