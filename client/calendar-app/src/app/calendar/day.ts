export interface IDay {
  value: number;
  description?: string;

  isToday(): boolean;
  isCurrentMonth(): boolean;
}

export class Day implements IDay {

  value: number;
  description?: string;

  private today: boolean;
  private current: boolean;

  constructor(value: number, today: boolean, current: boolean) {    
    this.value = value;
    this.today = today;
    this.current = current;
  }

  isToday(): boolean {
    return this.today;
  }
  isCurrentMonth(): boolean {
    return this.current;
  }
}